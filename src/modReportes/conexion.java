package modReportes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class conexion {
    public Connection abrirConexion() throws SQLException{
        Connection con;
        //Para conectarnos a nuestra base de datos
        try{
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            con = DriverManager.getConnection(
                    "jdbc:mysql://192.168.137.1:3306/bd_maquila","Hermose", "Hermose@321"); // Se establece la conexión
        }catch(SQLException e){
            System.out.println("NO se pudo abrir conexión");            
            con = null;
        }            
        return con;
    }
    
    public void cerrarConexion(Connection c) throws SQLException{        
        try{
            if(!c.isClosed()){
                c.close();
            }
        }catch(SQLException e){
            System.out.println("Error al cerrar la conexión");
        }        
    }
}
