// MODULO DE REPORTES (Controlador)

package modReportes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.x.protobuf.MysqlxNotice.Frame;

public class cReportes
{
    private vReportes vista;
    private mReportes modelo;

    private JComboBox<String> cbxPreguntas;
    private JButton btnCerrar, btnMinimizar;
    private JPanel grafica;


    // --------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------


    public cReportes(){
        vista = new vReportes(this);
        modelo = new mReportes();
        
        instanciarComponentes();
        agregarPreguntas();
    }

    private void instanciarComponentes(){
        cbxPreguntas = vista.cbxPregunta;
        cbxPreguntas.addActionListener(this.cambiarOpcion_cbxPreg);

        btnCerrar = vista.btnCerrar;
        btnCerrar.addActionListener(this.cerrarVentana);

        btnMinimizar = vista.btnMinimizar;
        btnMinimizar.addActionListener(this.minimizarVentana);

        grafica = vista.grafica;
    }

    private void  agregarPreguntas(){
        // Definir la opciones
        String strPlaceholder = "Seleccione un opción...";
        String strPregunta1 = "Según las ventas de esta temporada, ¿Qué tipo de mango presenta mejor rendimiento?";
        String strPregunta2 = "¿Qué clientes presentan mayor demanda y cu\u00E1l a sido su participación en las exportaciones?";
        String strPregunta3 = "¿Cuál ha sido la tendencia de producción de mangos durante los ultimos tres años por tipo y temporada?";

        // Agregar las opciones
        cbxPreguntas.addItem(strPlaceholder);
        cbxPreguntas.addItem(strPregunta1);
        cbxPreguntas.addItem(strPregunta2);
        cbxPreguntas.addItem(strPregunta3);
    }

    private ActionListener cambiarOpcion_cbxPreg = new ActionListener()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            int seleccion = cbxPreguntas.getSelectedIndex();

            switch(seleccion){
                case 0: vaciarPantalla(); break;
                case 1: iniciarPregunta1(); break;
                case 2: iniciarPregunta2(); break;
                case 3: iniciarPregunta3(); break;
            }
        }
    };


    // --------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------


    private ActionListener cerrarVentana = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            System.exit(0);
        }
    };
    private ActionListener minimizarVentana = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            vista.setExtendedState(Frame.TYPE_FIELD_NUMBER);
        }
    };


    // --------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------


    private void vaciarPantalla(){
        vista.vaciarTabla();
    }

    private void iniciarPregunta1(){
        final DefaultTableModel inf = modelo.RendimientoPerdidasPA1();
        vista.formatearTabla_pregunta1(inf);
    }

    private void iniciarPregunta2(){
        vista.formatearTabla_pregunta2();
    }
    private void iniciarPregunta3(){
        vista.formatearTabla_pregunta3();
    }
}
