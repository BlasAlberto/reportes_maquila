// MODULO DE REPORTES (vista)

package modReportes;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.util.Locale;

public class vReportes extends JFrame
{
    private cReportes controlador;

    // Paneles: Principales.
    private JPanel pFondo, pCabecera, pCuerpo;
    // Paneles: Barra superior o cabecera'.
    private JPanel pTitulo, pControlFrame;
    // Paneles: Cuerpo.
    private JPanel pContenido, pPregunta, pTabla, pGrafica;
    protected JPanel grafica;
    // Botones
    protected JButton btnMinimizar, btnCerrar;
    // Etiquetas de texto
    private JLabel lblTitulo, lblPregunta;
    // ComboBoxs
    protected JComboBox<String> cbxPregunta;
    // Tablas
    private JTable tbInf;
    private JTableHeader tbhInf;

    // Medidas
    private final int ANCHO_FRAME= 1020;
    private final int ALTO_FRAME= 600;
    private final int ALTO_CABECERA = 40;
    private final int ANCHO_CONTENIDO = (int)(ANCHO_FRAME/2);
    private final int ALTO_CONTENIDO = (ALTO_FRAME-ALTO_CABECERA);

    // Colores
    private final Color BLANCO = Color.WHITE;
    private final Color NEGRO = Color.BLACK;
    // Colores del formulario
    private final Color COLOR_FONDO = BLANCO;
    private final Color COLOR_PRIMARIO = new Color(218, 227, 243);
    private final Color COLOR_SECUNDARIO = new Color(180, 199, 231);

    // Tipos de bordes
    private final Border SIN_BORDES = BorderFactory.createLineBorder(COLOR_FONDO, 0);
    private final Border BORDE_SENCILLO = BorderFactory.createLineBorder(NEGRO, 1);

    // Fuentes
    private final Font fuenteGrandeBold = new Font("Segoe UI", 1, 18);
    private final Font fuenteGrande = new Font("Segoe UI", 0, 18);
    private final Font fuenteMedianaBold = new Font("Segoe UI", 1, 16);
    private final Font fuenteMediana = new Font("Segoe UI", 0, 16);
    private final Font fuenteChica = new Font("Segoe UI", 0, 14);

    // Lenguajes
    private final Locale espaniol = new Locale("es");

    // Rutas
    private final String RUTA_ACTUAL = System.getProperty("user.dir");

    // Iconos
    private final ImageIcon ICON_MINIMIZAR = new ImageIcon(RUTA_ACTUAL + "\\src\\img\\hidden.png");
    private final ImageIcon ICON_CERRAR = new ImageIcon(RUTA_ACTUAL + "\\src\\img\\closed.png");

    // Textos
    private final String strTitulo = "Reportes";
    private final String strPregunta = "Pregunta";

    // Tablas
    private DefaultTableModel tablaPregunta1 = new DefaultTableModel();
    private DefaultTableModel tablaPregunta2 = new DefaultTableModel();
    private DefaultTableModel tablaPregunta3 = new DefaultTableModel();


    // --------------------------------------------------------------------
    // --------------------------------------------------------------------
    
    
    public vReportes(cReportes controlador){
        this.controlador = controlador;

        // Inicar los componentes
        pintarVentana();
        // Mostrar ventana
        mostrarJPanel();
    }
    
    
    // --------------------------------------------------------------------
    // --------------------------------------------------------------------


    private void pintarVentana() {
        crear_PanelesPrincipales();
        crear_PanelesCabecera();
        crear_PanelesCuerpo();
    }
    private void mostrarJPanel() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(ANCHO_FRAME, ALTO_FRAME);
        setLocationRelativeTo(this);
        setUndecorated(true);
        setLayout(null);
        setVisible(true);
    }


    // --------------------------------------------------------------------
    // --------------------------------------------------------------------


    private void crear_PanelesPrincipales(){
        // Fondo
        pFondo = new JPanel();
        pFondo.setSize(ANCHO_FRAME, ALTO_FRAME);
        pFondo.setLocation(0, 0);
        pFondo.setBackground(COLOR_FONDO);
        pFondo.setLayout(null);
        this.add(pFondo);
        
        // Cabecera
        pCabecera = new JPanel();
        pCabecera.setSize(ANCHO_FRAME, ALTO_CABECERA);
        pCabecera.setLocation(0, 0);
        pCabecera.setBackground(COLOR_FONDO);
        pCabecera.setLayout(null);
        pFondo.add(pCabecera);

        // Cuerpo
        pCuerpo = new JPanel();
        pCuerpo.setSize(ANCHO_FRAME, (ALTO_FRAME-ALTO_CABECERA));
        pCuerpo.setLocation(0, ALTO_CABECERA);
        pCuerpo.setBackground(COLOR_FONDO);
        pCuerpo.setLayout(null);
        pFondo.add(pCuerpo);
    }

    private void crear_PanelesCabecera(){
        // Componentes extras
        JPanel pLinea1, pLinea2;

        // SECCIÓN DEL TITULO
        // Medidas
        final int ANCHO_pTITULO = (int)(ANCHO_FRAME*.9);
        final int ANCHO_LINEAS = (int)(ANCHO_pTITULO*.4);
        final int GROSOR_LINEAS = 1;
        final int MARGEN_pTITULO = (int)(ANCHO_pTITULO*0.02);
        final int ANCHO_TITULO = ANCHO_pTITULO-(MARGEN_pTITULO*2)-(ANCHO_LINEAS*2);
        // Posiciones
        final int Y_LINEAS = (int)(ALTO_CABECERA*.55);

        pTitulo = new JPanel();
        pTitulo.setBounds(
            0, 0,
            ANCHO_pTITULO, ALTO_CABECERA);
        pTitulo.setBackground(COLOR_FONDO);
        pTitulo.setLayout(null);
        pCabecera.add(pTitulo);

        pLinea1 = new JPanel();
        pLinea1.setBounds(
            MARGEN_pTITULO, Y_LINEAS,
            ANCHO_LINEAS, GROSOR_LINEAS);
        pLinea1.setBackground(NEGRO);
        pLinea1.setLayout(null);
        pTitulo.add(pLinea1);

        lblTitulo = new JLabel(strTitulo, JLabel.CENTER);
        lblTitulo.setFont(fuenteGrandeBold);
        lblTitulo.setBounds(
            MARGEN_pTITULO+ANCHO_LINEAS, 0,
            ANCHO_TITULO, ALTO_CABECERA);
        lblTitulo.setForeground(NEGRO);
        lblTitulo.setLayout(null);
        pTitulo.add(lblTitulo);

        pLinea2 = new JPanel();
        pLinea2.setBounds(
            MARGEN_pTITULO+ANCHO_LINEAS+ANCHO_TITULO, Y_LINEAS,
            ANCHO_LINEAS, GROSOR_LINEAS);
        pLinea2.setBackground(NEGRO);
        pLinea2.setLayout(null);
        pTitulo.add(pLinea2);


        // SECCIÓN DE BOTONES
        // Medidas
        final int ANCHO_pBOTONES = ANCHO_FRAME-ANCHO_pTITULO;
        final int ANCHO_BOTONES = 30;
        final int MARGEN_X_BOTONES = (int)((ANCHO_pBOTONES - (ANCHO_BOTONES*2))/3);
        final int MARGEN_Y_BOTONES = (int)((ALTO_CABECERA-ANCHO_BOTONES)/1.5);
        // Iconos
        Icon iconMinimizar = new ImageIcon(ICON_MINIMIZAR.getImage().getScaledInstance(
            ANCHO_BOTONES,
            ANCHO_BOTONES,
            Image.SCALE_SMOOTH
        ));
        Icon iconCerrar = new ImageIcon(ICON_CERRAR.getImage().getScaledInstance(
            ANCHO_BOTONES,
            ANCHO_BOTONES,
            Image.SCALE_SMOOTH
        ));

        pControlFrame = new JPanel();
        pControlFrame.setBackground(COLOR_FONDO);
        pControlFrame.setBounds(
            ANCHO_pTITULO, 0,
            (int)(ANCHO_FRAME-ANCHO_pTITULO), ALTO_CABECERA);
        pControlFrame.setLayout(null);
        pCabecera.add(pControlFrame);

        btnMinimizar = new JButton(iconMinimizar);
        btnMinimizar.setBackground(COLOR_FONDO);
        btnMinimizar.setToolTipText("Minimizar ventana");
        btnMinimizar.setFocusPainted(false);
        btnMinimizar.setBounds(
            MARGEN_X_BOTONES, MARGEN_Y_BOTONES,
            ANCHO_BOTONES, ANCHO_BOTONES);
        btnMinimizar.setBorder(SIN_BORDES);
        pControlFrame.add(btnMinimizar);

        btnCerrar = new JButton(iconCerrar);
        btnCerrar.setBackground(COLOR_FONDO);
        btnCerrar.setToolTipText("Cerrar ventana");
        btnCerrar.setFocusPainted(false);
        btnCerrar.setBounds(
            (MARGEN_X_BOTONES*2 + ANCHO_BOTONES), MARGEN_Y_BOTONES,
            ANCHO_BOTONES, ANCHO_BOTONES);
        btnCerrar.setBorder(SIN_BORDES);
        pControlFrame.add(btnCerrar);
    }

    private void crear_PanelesCuerpo(){
        // Medidas
        final int ALTO_CUERPO = ALTO_FRAME-ALTO_CABECERA;
        final int ALTO_pPREGUNTAS = 40;
        final int ANCHO_pPREGUNTAS = (int)(ANCHO_CONTENIDO*.94);
        final int ANCHO_lblPREGUNTA = (int)(ANCHO_pPREGUNTAS*.16);
        final int ANCHO_cbxPREGUNTA = (int)(ANCHO_pPREGUNTAS*.7);
        final int ALTO_TABLA = (int)(ALTO_CONTENIDO*.7);
        final int ALTO_GRAFICA = (int)(ALTO_CONTENIDO*.8);
        final int MARGEN_Y_CONTENIDO = (ALTO_CONTENIDO-ALTO_pPREGUNTAS-ALTO_TABLA)/5;
        final int MARGEN_X_CONTENIDO = (ANCHO_CONTENIDO-ANCHO_pPREGUNTAS)/2;
        final int MARGEN_X_PREGUNTAS = (ANCHO_pPREGUNTAS-ANCHO_lblPREGUNTA-ANCHO_cbxPREGUNTA)/5;
        final int MARGEN_Y_GRAFICA = (ALTO_CONTENIDO-ALTO_GRAFICA)/2;

        // SECCIÓN DE CONTENIDO
        pContenido = new JPanel();
        pContenido.setBounds(
            0, 0,
            ANCHO_CONTENIDO, ALTO_CONTENIDO);
        pContenido.setBackground(COLOR_FONDO);
        pContenido.setLayout(null);
        pCuerpo.add(pContenido);

        pPregunta = new JPanel();
        pPregunta.setBounds(
            MARGEN_X_CONTENIDO, MARGEN_Y_CONTENIDO*2,
            ANCHO_pPREGUNTAS, ALTO_pPREGUNTAS);
        pPregunta.setBackground(COLOR_FONDO);
        pPregunta.setLayout(null);
        pContenido.add(pPregunta);

        lblPregunta = new JLabel(strPregunta, JLabel.RIGHT);
        lblPregunta.setFont(fuenteMediana);
        lblPregunta.setBounds(
            (MARGEN_X_PREGUNTAS*2), 0,
            ANCHO_lblPREGUNTA, ALTO_pPREGUNTAS);
        lblPregunta.setForeground(NEGRO);
        lblPregunta.setLayout(null);
        pPregunta.add(lblPregunta);

        cbxPregunta = new JComboBox<>();
        cbxPregunta.setBounds(
            ((MARGEN_X_PREGUNTAS*3)+ANCHO_lblPREGUNTA), 0,
            ANCHO_cbxPREGUNTA, ALTO_pPREGUNTAS);
        cbxPregunta.setUI(new BasicComboBoxUI(){
            protected JButton createArrowButton() {
                final JButton button = new JButton("▼");
                button.setName("ComboBox.arrowButton");
                button.setFocusPainted(false);
                button.setBorder(SIN_BORDES);
                button.setFont(fuenteGrande);
                return button;
            }
            public void configureArrowButton() {
                super.configureArrowButton();
                arrowButton.setBackground(COLOR_FONDO);
                arrowButton.setForeground(COLOR_SECUNDARIO);
            }
        });
        cbxPregunta.setFont(fuenteGrande);
        cbxPregunta.setForeground(NEGRO);
        cbxPregunta.setBackground(COLOR_FONDO);
        cbxPregunta.setFocusable(false);
        cbxPregunta.setBorder(BORDE_SENCILLO);
        ((JLabel)cbxPregunta.getRenderer()).setHorizontalAlignment(SwingConstants.LEFT);
        pPregunta.add(cbxPregunta);

        pTabla = new JPanel();
        pTabla.setBounds(
            MARGEN_X_CONTENIDO, (MARGEN_Y_CONTENIDO*3)+ALTO_pPREGUNTAS,
            ANCHO_pPREGUNTAS, ALTO_TABLA);
        pTabla.setBackground(COLOR_FONDO);
        pTabla.setBorder(BORDE_SENCILLO);
        pTabla.setLayout(null);
        pContenido.add(pTabla);

        tbInf = new JTable();
        tbInf.setBounds(
            0, 30,
            ANCHO_pPREGUNTAS, ALTO_TABLA);
        tbInf.setBackground(COLOR_PRIMARIO);
        tbInf.setForeground(NEGRO);
        tbInf.setFont(fuenteMediana);
        tbInf.setRowHeight(25);
        pTabla.add(tbInf);

        tbhInf = tbInf.getTableHeader();
        tbhInf.setBounds(
            0, 0,
            ANCHO_pPREGUNTAS, 30);
        tbhInf.setBackground(COLOR_PRIMARIO);
        tbhInf.setForeground(NEGRO);
        tbhInf.setFont(fuenteChica);
        pTabla.add(tbhInf);
        // tbInsumos.setSize(psTablaDetalles.getWidth(), tbInsumos.getRowCount()*25);
        // pTablaDetalles.setPreferredSize(
        //     new Dimension(0, tbhInsumos.getHeight() + tbInsumos.getHeight()));


        // SECCIÓN DE GRAFICADO
        pGrafica = new JPanel();
        pGrafica.setBounds(
            ANCHO_CONTENIDO, 0,
            ANCHO_CONTENIDO, ALTO_CONTENIDO);
        pGrafica.setBackground(COLOR_FONDO);
        pGrafica.setLayout(null);
        pCuerpo.add(pGrafica);

        grafica = new JPanel();
        grafica.setBounds(
            MARGEN_X_CONTENIDO, MARGEN_Y_GRAFICA,
            ANCHO_pPREGUNTAS, ALTO_GRAFICA);
        grafica.setBackground(COLOR_FONDO);
        grafica.setBorder(BORDE_SENCILLO);
        grafica.setLayout(null);
        pGrafica.add(grafica);
    }

    public void vaciarTabla(){
        DefaultTableModel tablaDetalles = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int filas, int columnas){
                // Deshabilito la edición de celdas
                return false;
            }
        };
        
        tablaDetalles.setColumnIdentifiers(new String[] {});
        tbInf.setModel(tablaDetalles);
    }

    public void formatearTabla_pregunta1(){
        tablaPregunta1 = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int filas, int columnas){
                // Deshabilito la edición de celdas
                return false;
            }
        };
        
        tablaPregunta1.setColumnIdentifiers(new String[] {
            "Tipo",
            "t Vendidas",
            "C Vendidas",
            "t Entregadas",
            "t Perdidas"});
        tbInf.setModel(tablaPregunta1);
    }

    public void formatearTabla_pregunta1(DefaultTableModel tabla){

        final String nombreColumna1 = "Tipo";
        final String nombreColumna2 = "t Vendidas";
        final String nombreColumna3 = "C Vendidas";
        final String nombreColumna4 = "t Entregadas";
        final String nombreColumna5 = "t Perdidas";

        final int nColumnas = tabla.getColumnCount();
        final int nFilas = tabla.getRowCount();

        if(nColumnas == 5)
        {
            // SÍ COINCIDEN LAS TABLAS
            // Crear la tabla y asignarle las columnas.
            tablaPregunta1 = new DefaultTableModel(){
                @Override
                public boolean isCellEditable(int filas, int columnas){
                    // Deshabilito la edición de celdas
                    return false;
                }
            };
            tablaPregunta1.setColumnIdentifiers(new String[] {
                nombreColumna1,
                nombreColumna2,
                nombreColumna3,
                nombreColumna4,
                nombreColumna5});

            // Copiar el contenido / las filas.
            for(int i=0; i<nFilas; i++){
                final Object[] fila = {
                    tabla.getValueAt(i, 0),
                    tabla.getValueAt(i, 1),
                    tabla.getValueAt(i, 2),
                    tabla.getValueAt(i, 3),
                    tabla.getValueAt(i, 4)
                };
                tablaPregunta1.addRow(fila);
            }
            
            // Mostrar tabla.
            tbInf.setModel(tablaPregunta1);
        }

        else formatearTabla_pregunta1();
    }

    public void formatearTabla_pregunta2(){
        tablaPregunta2 = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int filas, int columnas){
                // Deshabilito la edición de celdas
                return false;
            }
        };
        
        tablaPregunta2.setColumnIdentifiers(new String[] {
            "Nombre",
            "t Demandadas",
            "Participación en exportaciones"});
        tbInf.setModel(tablaPregunta2);
    }

    public void formatearTabla_pregunta3(){
        tablaPregunta3 = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int filas, int columnas){
                // Deshabilito la edición de celdas
                return false;
            }
        };
        
        tablaPregunta3.setColumnIdentifiers(new String[] {
            "Tipo",
            "t Producidas",
            "Año"});
        tbInf.setModel(tablaPregunta3);
    }
}
